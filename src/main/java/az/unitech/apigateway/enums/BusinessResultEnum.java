package az.unitech.apigateway.enums;

import org.springframework.http.HttpStatus;

public enum BusinessResultEnum {

    ACCESS_TOKEN_EXPIRED(10001, HttpStatus.UNAUTHORIZED,"Access token expired"),
    UNAUTHORIZED(10002, HttpStatus.UNAUTHORIZED,"Authorization required"),
    INVALID_JWT_TOKEN(10003, HttpStatus.UNAUTHORIZED,"JWT Token is invalid"),
    INTERNAL_ERROR(999, HttpStatus.INTERNAL_SERVER_ERROR,"Internal Server Error");

    private final Integer code;
    private final HttpStatus httpStatus;
    private final String message;

    BusinessResultEnum(Integer code, HttpStatus httpStatus, String message) {
        this.code = code;
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }
}
