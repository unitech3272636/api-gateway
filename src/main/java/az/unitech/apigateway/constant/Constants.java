package az.unitech.apigateway.constant;

public class Constants {
    public static final String AUTHORIZATION = "Authorization";
    public static final String X_USER_ID = "X-User-ID";
    public static final String USER_ID = "user_id";
    public static final String REGISTER_ENDPOINT = "/register";
    public static final String LOGIN_ENDPOINT = "/login";
    public static final String ALGORITHM_RSA = "RSA";


}