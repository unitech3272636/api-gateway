package az.unitech.apigateway.config;

import az.unitech.apigateway.filter.JwtAuthenticationFilter;
import lombok.AllArgsConstructor;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class GatewayConfig {
    private final JwtAuthenticationFilter filter;
    private final UrlProperties urlProperties;
    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("auth", r -> r.path("/auth/**")
                        .uri(urlProperties.getAuthService()))
                .route("accounts", r -> r.path("/accounts/**")
                        .filters(f -> f.filter(filter))
                        .uri(urlProperties.getAccountService()))
                .build();
    }
}
