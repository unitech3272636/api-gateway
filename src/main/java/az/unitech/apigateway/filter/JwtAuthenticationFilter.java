package az.unitech.apigateway.filter;
import java.util.List;
import java.util.function.Predicate;

import az.unitech.apigateway.enums.BusinessResultEnum;
import az.unitech.apigateway.exception.CustomException;
import az.unitech.apigateway.util.JwtUtil;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import io.jsonwebtoken.Claims;
import reactor.core.publisher.Mono;


import static az.unitech.apigateway.constant.Constants.*;

@Component
@AllArgsConstructor
public class JwtAuthenticationFilter implements GatewayFilter {

    private final JwtUtil jwtUtil;

    @SneakyThrows
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        ServerHttpRequest request = exchange.getRequest();

        final List<String> apiEndpoints = List.of(REGISTER_ENDPOINT, LOGIN_ENDPOINT);

        Predicate<ServerHttpRequest> isApiSecured = r -> apiEndpoints.stream()
                .noneMatch(uri -> r.getURI().getPath().contains(uri));

        if (isApiSecured.test(request)) {

            if (!request.getHeaders().containsKey(AUTHORIZATION)) {
                return Mono.error(CustomException.of(BusinessResultEnum.UNAUTHORIZED));
            }

            final String token = request.getHeaders().getOrEmpty(AUTHORIZATION).get(0);

            try {
                jwtUtil.validateToken(token);
            } catch (ExpiredJwtException e) {
                return Mono.error(CustomException.of(BusinessResultEnum.ACCESS_TOKEN_EXPIRED));
            }catch (Exception exception){
                return Mono.error(CustomException.of(BusinessResultEnum.INVALID_JWT_TOKEN));
            }

            Claims claims = jwtUtil.getAllClaimsFromToken(token);
            exchange.getRequest().mutate().header(X_USER_ID, String.valueOf(claims.get(USER_ID))).build();
        }

        return chain.filter(exchange);
    }
}
