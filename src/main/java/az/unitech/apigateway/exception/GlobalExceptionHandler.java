//package az.unitech.apigateway.exception;
//
//import az.unitech.apigateway.enums.BusinessResultEnum;
//import io.jsonwebtoken.ExpiredJwtException;
//import lombok.AllArgsConstructor;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
//
//@ControllerAdvice
//@AllArgsConstructor
//public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
//
//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
//    public final CommonErrorResponse handleUnknownException(Exception ex) {
//        return CommonErrorResponse.newInstance(ex.getMessage(), BusinessResultEnum.INTERNAL_ERROR);
//    }
//
//    @ExceptionHandler(ExpiredJwtException.class)
//    @ResponseStatus(HttpStatus.UNAUTHORIZED)
//    public CommonErrorResponse unknownException(ExpiredJwtException exception) {
//        exception.printStackTrace();
//        return CommonErrorResponse.newInstance(exception.getMessage(), BusinessResultEnum.ACCESS_TOKEN_EXPIRED);
//    }
//}
//
