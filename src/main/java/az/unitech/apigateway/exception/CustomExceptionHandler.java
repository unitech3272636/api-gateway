package az.unitech.apigateway.exception;

import az.unitech.apigateway.enums.BusinessResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.DefaultErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@Slf4j
public class CustomExceptionHandler extends DefaultErrorWebExceptionHandler {

    public CustomExceptionHandler(ErrorAttributes errorAttributes, WebProperties.Resources resources,
                                  ErrorProperties errorProperties, ApplicationContext applicationContext) {
        super(errorAttributes, resources, errorProperties, applicationContext);
    }

    @Override
    protected Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
        Throwable throwable = getError(request);
        if (throwable != null && throwable.getCause() != null)
            log.error("Failure, cause was : ", throwable.getCause());
        if (throwable instanceof CustomException) {
            CustomException businessException = (CustomException) throwable;
            return buildResponse(businessException);
        } else if (throwable instanceof ResponseStatusException) {
            ResponseStatusException responseStatusException = (ResponseStatusException) throwable;
            if (HttpStatus.NOT_FOUND.equals(responseStatusException.getStatus())) {
                return buildNotFoundResponse(responseStatusException);
            }
        }
        return buildResponse(throwable);
    }

    private Mono<ServerResponse> buildResponse(CustomException customException) {
        CommonErrorResponse commonErrorResponse =
                CommonErrorResponse.newInstance(customException.getBusinessResultEnum());
        return ServerResponse.status(customException.getBusinessResultEnum().getHttpStatus())
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(commonErrorResponse));
    }

    private Mono<ServerResponse> buildNotFoundResponse(Throwable throwable) {
        log.error("Not found exception: {}", throwable.getMessage());
        CommonErrorResponse commonErrorResponse =
                CommonErrorResponse.newInstance(BusinessResultEnum.INTERNAL_ERROR);
        return ServerResponse.status(BusinessResultEnum.INTERNAL_ERROR.getHttpStatus())
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(commonErrorResponse));
    }

    private Mono<ServerResponse> buildResponse(Throwable throwable) {
        log.error("Unknown exception: ", throwable);
        CommonErrorResponse commonErrorResponse =
                CommonErrorResponse.newInstance(BusinessResultEnum.INTERNAL_ERROR);
        return ServerResponse.status(BusinessResultEnum.INTERNAL_ERROR.getHttpStatus())
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(commonErrorResponse));
    }

}

