package az.unitech.apigateway.exception;

import az.unitech.apigateway.enums.BusinessResultEnum;

import java.io.Serializable;

public class CustomException extends RuntimeException implements Serializable {

    private BusinessResultEnum businessResultEnum;

    public CustomException(BusinessResultEnum businessResultEnum) {
        this.businessResultEnum = businessResultEnum;
    }

    public BusinessResultEnum getBusinessResultEnum() {
        return businessResultEnum;
    }

    public static CustomException of(BusinessResultEnum businessResultEnum) {
        return new CustomException(businessResultEnum);
    }

    public void throwEx() {
        throw this;
    }
}
